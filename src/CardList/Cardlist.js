import Message from "../script/modules/MessegeGenerator"
import api from "../script/modules/api"
import { CreatVisitButton, LogOutButton } from "./LogOut__CreateVisit-btns"
import {CardDentist, CardCardio, CardTerapist} from "../card/cardClasses";
import FilterForm from "../Filter/FilterForm"


export default class CardList {
    static  parent = document.querySelector(".card-list")
    static userCards

    static draggedItem
    static droppedItem

    static async render(){
        if (!document.getElementById("CreatVisitButton") && !document.getElementById("logOut")){
            new CreatVisitButton("CreatVisitButton", "Создать визит").render()
            new LogOutButton("logOutBtn", "logOut").render()
        }
        await CardList.showUsersCards()
    }

    static async showUsersCards(arr){
        try {
            if (!arr) {
            await CardList.setUserCards()
            arr = CardList.userCards
            }
            if (!document.querySelector("#filter")) new  FilterForm().render()
            arr.forEach(cardData => {
                if (cardData.doctor === "cardiologist") return new CardCardio(cardData, "beforeend").render()
                if (cardData.doctor === "dentist") return new CardDentist(cardData, "beforeend").render()
                if (cardData.doctor === "therapist") return new CardTerapist(cardData, "beforeend").render()
            })
            CardList.dragAndDrop()
        }catch(e){
            console.error(e)
            alert("Sorry, server is not responding. Try later")
        }
    }

     static destroyLoginBtn(){
        document.querySelector("#logIn").remove()
    }

    static async setUserCards(){
        Message.delete(CardList.parent)
        CardList.userCards =  await api.getCards().then(r=> r.json())
        if ( !CardList.userCards || CardList.userCards.length <=0 ) return Message.showMessage(this.parent, "afterbegin", "У вас нет активных записей")
    }

    static isThereAnyCard(){
        const showedCards = document.querySelector(".card")
        Message.delete()
    }

    static async filter({doctor, description, priority}){
        await  CardList.setUserCards()

        if (CardList.userCards.length <= 0 || !CardList.userCards) return
        CardList.cleanCardList()
        let filtered = CardList.userCards.filter(card => {
            if(!doctor && !description && !priority) return card

            if(doctor && description && priority){
                if ( card.doctor === doctor && card.description.includes(description) && card.priority === priority ) return card
            }
            if(doctor && description && !priority){
                if ( card.doctor === doctor && card.description.includes(description) ) return card
            }
            if(doctor && !description && priority){
                if ( card.doctor === doctor && card.priority === priority ) return card
            }
            if(!doctor && description && priority){
                if (card.description.includes(description) && card.priority === priority ) return card
            }
            if(doctor && !description && !priority){
                if ( card.doctor === doctor ) return card
            }
            if(!doctor && description && !priority){
                if ( card.description.includes(description)) return card
            }
            if(!doctor && !description && priority){
                if ( card.priority === priority ) return card
            }
        })
        if (filtered.length < 1) Message.showMessage(CardList.parent, "afterbegin", "Совпадений не найдено")
       await CardList.showUsersCards(filtered)
    }

    static cleanCardList(){
        document.querySelectorAll(".card").forEach(card => card.remove())
    }

    static dragAndDrop() {
        const dragItems = document.querySelectorAll(".drag-item")
        const dropZone = document.querySelector(".drop-zone")
        dragItems.forEach(card => {
            card.addEventListener("dragstart", () => CardList.draggedItem = card )
            card.addEventListener("dragend", () => CardList.draggedItem = null )
            card.addEventListener("dragenter",  () => CardList.droppedItem = card )
        })
        dropZone.addEventListener("dragleave", ev=> ev.preventDefault())
        dropZone.addEventListener("dragover",ev=> ev.preventDefault())
        dropZone.addEventListener("drop", CardList.handleDrop)
    }

    static handleDrop(){
        if (CardList.droppedItem){
            const children = [...this.children]
            let dragnetIndex = children.indexOf(CardList.draggedItem)
            let droppedIndex = children.indexOf(CardList.droppedItem)
            if(dragnetIndex > droppedIndex){
                this.insertBefore(CardList.draggedItem, CardList.droppedItem)
            }else {
                this.insertBefore(CardList.draggedItem, CardList.droppedItem.nextElementSibling)
            }
        }
    }
}



