import Button from "../script/modules/Button"
import User from "../script/modules/User.js";
import Message from "../script/modules/MessegeGenerator"
import VisitSelect from '../script/modules/visit/VisitSelect.js'
import Modal from '../script/modules/Modal.js'
import FilterForm from "../Filter/FilterForm";

class CreatVisitButton extends Button {
    constructor(buttonID, text) {
        super(buttonID, text);
    }

    render(){
        this.button.addEventListener("click", e=> {
            e.preventDefault()
            let visitSelect = new VisitSelect().createSelect()
             new Modal(visitSelect).render()
        })
        super.render()
    }
}

class LogOutButton extends Button {
    constructor(buttonID, text) {
        super(buttonID, text)
    }

    render() {
        this.button.addEventListener("click", async e=>{
            e.preventDefault()
           await this.handleClick()
        })
        super.render()
    }

  async handleClick(){
        this.removeAll()
        await new User().start()
    }
    removeAll(){
        document.querySelector("#CreatVisitButton").remove()
        document.querySelectorAll(".card").forEach(el => el.remove())
        this.button.remove()
        Message.delete()
        FilterForm.deleteFilterForm()
        localStorage.removeItem("userToken")
    }
}

export {CreatVisitButton, LogOutButton}
