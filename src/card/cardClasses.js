import Card from "./card.js";
import Modal from "../script/modules/Modal.js";
import VisitDentistUpdate from "../script/modules/visit/classes/VisitDentistUpdate.js";
import VisitCardiologistUpdate from "../script/modules/visit/classes/VisitCardiologistUpdate.js";
import VisitTherapistUpdate from "../script/modules/visit/classes/VisitTherapistUpdate.js";

class CardCardio extends Card {
  constructor(
    {
      userName,
      title,
      doctor,
      goal,
      description,
      priority,
      pressure,
      weightIndex,
      illness,
      age,
      id,
    },
    location
  ) {
    super({ userName, title, id }, location);
    this.doctor = doctor;
    this.goal = goal;
    this.description = description || "";
    this.priority = priority;
    this.pressure = pressure;
    this.weightIndex = weightIndex;
    this.illness = illness;
    this.age = age;
  }

  showMore() {
    super.showMore();
    const { itemsList } = this._ELEMENTS;
    itemsList.insertAdjacentHTML(
      "beforeend",
      ` <li class="card-item"><span>Доктор:</span> ${this.doctor}</li>
      <li class="card-item"> <span>Цель визита:</span> ${this.goal}</li>
<li class="card-item"> <span>Описание визита:</span> ${this.description}</li>
<li class="card-item"><span>Срочность:</span> ${this.choosePriority()}</li>
<li class="card-item"> <span>Аретриальное давление:</span> ${this.pressure}</li>
<li class="card-item"> <span>Индекс массы тела:</span> ${this.weightIndex}</li>
<li class="card-item"><span>Перенесенные заболевания С.С.С. :</span> ${
        this.illness
      }</li>
<li class="card-item"> <span>Возраст: </span>${this.age}</li>`
    );
  }
  cardEdit() {
    let {
        userName,
        title,
        doctor,
        goal,
        description,
        priority,
        pressure,
        weightIndex,
        illness,
        age,
        id,
      } = this,
      cardEdit = new VisitCardiologistUpdate({
        userName,
        title,
        doctor,
        goal,
        description,
        priority,
        pressure,
        weightIndex,
        illness,
        age,
        id,
      });
    new Modal(cardEdit.createForm()).render();
  }
}

class CardDentist extends Card {
  constructor(
    { userName, title, doctor, goal, description, priority, lastVisitDate, id },
    location
  ) {
    super({ userName, title, id }, location);
    this.doctor = doctor;
    this.goal = goal;
    this.description = description || "";
    this.priority = priority;
    this.lastVisitDate = lastVisitDate;
  }

  showMore() {
    super.showMore();
    const { itemsList } = this._ELEMENTS;
    itemsList.insertAdjacentHTML(
      "beforeend",

      `<li class="card-item"><span>Доктор:</span> ${this.doctor}</li>
      <li class="card-item"><span>Цель визита:</span> ${this.goal}</li>
      <li class="card-item"><span>Описание визита:</span> ${
        this.description
      }</li>
<li class="card-item"><span>Срочность:</span> ${this.choosePriority()}</li>
<li class="card-item"><span>Дата последнего визита:</span> ${
        this.lastVisitDate
      }</li>`
    );
  }
  cardEdit() {
    let {
      userName,
      title,
      doctor,
      goal,
      description,
      priority,
      lastVisitDate,
      id,
    } = this;
    let cardEdit = new VisitDentistUpdate({
      userName,
      title,
      doctor,
      goal,
      description,
      priority,
      lastVisitDate,
      id,
    });
    new Modal(cardEdit.createForm()).render();
  }
}
class CardTerapist extends Card {
  constructor(
    { userName, title, doctor, goal, description, priority, age, id },
    location
  ) {
    super({ userName, title, id }, location);
    this.doctor = doctor;
    this.goal = goal;
    this.description = description || "";
    this.priority = priority;
    this.age = age;
  }

  showMore() {
    super.showMore();
    const { itemsList } = this._ELEMENTS;
    itemsList.insertAdjacentHTML(
      "beforeend",
      `<li class="card-item"><span>Доктор:</span> ${this.doctor}</li>
  <li class="card-item"> <span>Цель визита:</span>${this.goal}</li>
<li class="card-item"> <span>Описание визита:</span> ${this.description}</li>
      <li class="card-item"><span>Срочность:</span> ${this.choosePriority()}</li>
<li class="card-item"><span>Возраст:</span> ${this.age}</li>`
    );
   
  }
  cardEdit() {
    let { doctor, goal, description, priority, title, userName, age, id } =
        this,
      cardEdit = new VisitTherapistUpdate({
        doctor,
        goal,
        description,
        priority,
        title,
        userName,
        age,
        id,
      });
    new Modal(cardEdit.createForm()).render();
  }
}

export { CardCardio, CardDentist, CardTerapist };
