import Message from "../script/modules/MessegeGenerator";
import intro  from "../sound/sound.mp3";
import api from "../script/modules/api";
import ClassList from "../CardList/Cardlist";
import Modal from "../script/modules/Modal";

class LoginForm {
    _ELEMENTS = {
        form: document.createElement("form"),
        inputEmail: document.createElement("input"),
        inputPass: document.createElement("input"),
        labelEmail:document.createElement("label"),
        labelPass:document.createElement("label"),
        button: document.createElement("button"),
        testEmail: document.createElement("p"),
        testPass: document.createElement("p"),
    }

    constructor () {
    }

    createForm() {
        const {form, inputEmail, inputPass, button, labelEmail, labelPass, testEmail, testPass} = this._ELEMENTS

        form.setAttribute("id", "registration-form")

        labelEmail.setAttribute("for", "InputEmail")
        labelEmail.textContent = "Email"

        inputEmail.type = "email"
        inputEmail.className = "form-control mt-2"
        inputEmail.id = "InputEmail"
        inputEmail.placeholder="Enter email"

        labelPass.setAttribute("for", "InputPass")
        labelPass.textContent = "Password"
        labelPass.className ="mt-xxl-3 mt-xl-4 mt-3"

        inputPass.type = "password"
        inputPass.className ="form-control mt-2"
        inputPass.id = "InputPass"
        inputPass.placeholder="Enter password"
        inputPass.setAttribute("autocomplete", "on")

        button.type = "submit"
        button.className = "col-xxl-12 mt-xxl-3 mt-xl-4 mt-3 btn btn-primary"
        button.textContent = "Login"

        testEmail.className = "mt-3"
        testEmail.textContent = "Test email:  _test_@email.com"
        testPass.textContent = "Test password:  0000"

        form.append(labelEmail , inputEmail, labelPass, inputPass, button, testEmail, testPass)

        form.addEventListener("submit",   async e => {
            e.preventDefault()
            Message.delete(form)
            localStorage.removeItem("userToken")
            try{
                await this.handleSubmit()
            }catch (e){
                console.error(e)
            }
        })

        inputEmail.addEventListener("input", ()=>Message.delete(form))
        inputPass.addEventListener("input", ()=>Message.delete(form))

        return form
    }

    async handleSubmit(){
        const {form} = this._ELEMENTS

        try{
            await this.validUserData()
            ClassList.destroyLoginBtn()
            console.log(intro);
            let sound = new Audio(intro)
            sound.addEventListener("canplaythrough", ()=> sound.play())
            await ClassList.render()
            Modal.destroy()
        }catch(err){
            Message.showError(form, "beforeend", "Wrong password or email")
        }
    }

    async validUserData(){
        const {inputEmail, inputPass} = this._ELEMENTS
        if (inputEmail.value.trim().length < 5 || inputPass.value.trim().length < 1) throw new Error("Wrong password or email")

        const userData = await api.auth(this.#getUserData()).then(r => {
            if (r.status >= 400) throw new Error("Wrong data")
            return r.text()
        })
        localStorage.setItem('userToken',userData)
    }

    #getUserData(){
        const { inputEmail, inputPass} = this._ELEMENTS
        return {
            email: inputEmail.value,
            password: inputPass.value,
        }
    }
}

export {LoginForm}