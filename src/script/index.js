import "../style/index.scss";
import { logoSvg } from "./modules/svg.js"; //подключение svg
import User from "./modules/User.js";

window.onload = async () => {
  logoSvg(document.getElementById("logo"));
  await new User().start();
};
