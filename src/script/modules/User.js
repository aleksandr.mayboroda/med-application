import { LogInButton } from "../../login/LogIn-Btn"
import CardList from "../../CardList/Cardlist"


export default class User {
    isUserLogin = this.isLogin()
    isLogin(){
       return (!!localStorage.getItem("userToken"))
    }
    async start(){
        if (!this.isUserLogin && !document.getElementById("logIn")) return new LogInButton("logIn", "LogIn").render()
        await CardList.render()
    }
}
