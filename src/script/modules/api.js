class API {
    static headers = {
        'Content-Type': 'application/json',
    }
    static authHeaders = {
        ...API.headers,
        Authorization: `Bearer ${localStorage.getItem('userToken')}`,
    }

    static  auth(user){
     return   fetch('https://ajax.test-danit.com/api/v2/cards/login',{
            method: 'post',
            headers: API.headers,
            body: JSON.stringify(user)
        })
    }
    static getCards(){
        return   fetch('https://ajax.test-danit.com/api/v2/cards',{
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('userToken')}`,
        },
            method: 'GET',
        })
    }
    static getCard(id){
        return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`,{
            method: 'GET',
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('userToken')}`,
        },
        })
    }

    static addCard(card) {
        return fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: API.authHeaders,
            body: JSON.stringify(card)
        })
    }
    static updateCard(card) {
        return fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
            method: 'PUT',
            headers: API.authHeaders,
            body: JSON.stringify(card)
        })
    }
    static deleteCard(id) {
        return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
            method: 'DELETE',
            headers: {  'Authorization': `Bearer ${localStorage.getItem('userToken')}`},
        })
    }

}

export default API
