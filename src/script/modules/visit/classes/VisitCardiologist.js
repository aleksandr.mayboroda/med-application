import Visit from "./Visit.js";
// import API from "../../api.js";
// import Modal from "../../Modal.js";
// import Message from "../../MessegeGenerator.js";
// import { CardCardio } from "../../../card/cardClasses.js";

export default class VisitCardiologist extends Visit {
  constructor() {
    super();

    this._ELEMENTS = {
      ...this._ELEMENTS,
      pressureLabel: document.createElement("label"),
      pressure: document.createElement("input"), //давление
      weightIndexLabel: document.createElement("label"),
      weightIndex: document.createElement("input"), //индекс массы
      sorenessLabel: document.createElement("label"),
      illness: document.createElement("textarea"), //болезненность
      ageLabel: document.createElement("label"),
      age: document.createElement("input"), //возраст
    };
    this._ASSETS = {
      ...this._ASSETS,
      pressure: {
        className: "form-control mb-3",
        name: "pressure",
        id: "pressure",
        type: "text",
        attributes: [
          { name: "placeHolder", value: "Укажите обычное давление" },
        ],
      },
      pressureLabel: {
        className: "form-label mb-1",
        innerText: "Обычное Давление",
        attributes: [{ name: "for", value: "pressure" }],
      },
      weightIndex: {
        className: "form-control mb-3",
        name: "weightIndex",
        id: "weightIndex",
        type: "text",
        attributes: [
          { name: "placeHolder", value: "Укажите индекс массы тела" },
        ],
      },
      weightIndexLabel: {
        className: "form-label mb-1",
        innerText: "Индекс Массы Тела",
        attributes: [{ name: "for", value: "weightIndex" }],
      },
      sorenessLabel: {
        className: "form-label mb-1",
        innerText: "Перенесенные заболевания сердечно-сосудистой системы:",
        attributes: [{ name: "for", value: "illness" }],
      },
      illness: {
        name: "illness",
        id: "illness",
        className: "form-control mb-3",
        attributes: [{ name: "placeHolder", value: "Укажите заболевания" }],
      },
      age: {
        className: "form-control mb-3",
        name: "age",
        id: "age",
        type: "text",
        attributes: [{ name: "placeHolder", value: "Возраст" }],
      },
      ageLabel: {
        className: "form-label mb-1",
        innerText: "Укажите возраст (полных лет)",
        attributes: [{ name: "for", value: "age" }],
      },
    };
  }

  formValidate(formData) {
    super.formValidate(formData);
    let { pressure, weightIndex, illness, age } = this._FORM_INPUTS;
    if (!pressure || pressure.search(/^[\d]{2,3}\/[\d]{2,3}$/) === -1) {
      this._VALIDATE_ERRORS["pressure"] =
        'Не корректное давление - должно быть в формате  "120/70"';
    }
    if (isNaN(weightIndex) || +weightIndex <= 0) {
      this._VALIDATE_ERRORS["weightIndex"] =
        "Не корректный индекс массы тела - должно быть число больше 0";
    }
    if (!illness || illness.trim().length < 3) {
      this._VALIDATE_ERRORS["illness"] =
        "Описание заболеваний сердечно-сосудистой системы должно быть не короче 3 символов";
    }
    if (isNaN(age) || +age <= 0 || +age > 100 || +age % 1 !== 0) {
      this._VALIDATE_ERRORS["age"] =
        "Не корректный возраст - должно быть целое число от 1 до 100";
    }

    return (this._IS_VALID =
      Object.keys(this._VALIDATE_ERRORS).length > 0 ? false : true);
  }
}
