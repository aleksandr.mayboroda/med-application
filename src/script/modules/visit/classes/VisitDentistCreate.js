import VisitDentist from "./VisitDentist.js";
import API from "../../api.js";
import Modal from "../../Modal.js";
import Message from "../../MessegeGenerator.js";
import {CardDentist} from '../../../../card/cardClasses.js'
import CardList from "../../../../CardList/Cardlist";

export default class VisitDentistCreate extends VisitDentist
{
    constructor(doctor) {
        super();
        this.additionaValues = {
          doctor,
          title: "Визит к Стоматологу",
        };
    }

    async formSubmit(formData) {
        let form = this._ELEMENTS._form;
        Message.delete(form);
    
        if (!this.formValidate(formData)) {
          //вывод ошибок
          for (let err in this._VALIDATE_ERRORS) {
            Message.showError(form, "beforeend", this._VALIDATE_ERRORS[err]);
          }
        } else {
          // 2.1 отправка данных на сервер - переписать под AWAIT
          try {
            let { doctor, title } = this.additionaValues;
            let response = await API.addCard({
              title,
              doctor,
              ...this._FORM_INPUTS,
            }).then((resp) => resp.json());
            new CardDentist(response, "afterbegin").render();
              CardList.dragAndDrop()
          } catch (er) {
            alert("Sorry, try later");
          }
    
          //скрываем popup
          Modal.destroy();
        }
      }
}