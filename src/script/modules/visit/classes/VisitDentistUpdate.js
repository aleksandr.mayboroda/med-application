import VisitDentist from "./VisitDentist.js";
import API from "../../api.js";
import Modal from "../../Modal.js";
import Message from "../../MessegeGenerator.js";
import CardList from "../../../../CardList/Cardlist.js";

export default class VisitDentistUpdate extends VisitDentist {
  constructor(fields) {
    super(fields);
    this.fields = fields;
    this.setValues(fields);
    this._ASSETS.___submit.className = "btn btn-primary me-1";
  }

  async formSubmit(formData) {
    let form = this._ELEMENTS._form;

    Message.delete(form);
    if (!this.formValidate(formData)) {
      for (let err in this._VALIDATE_ERRORS) {
        Message.showError(form, "beforeend", this._VALIDATE_ERRORS[err]);
      }
    } else {
      try {
        let response = await API.updateCard({
          ...this.fields,
          ...this._FORM_INPUTS,
        }).then((resp) => resp.json());
        CardList.cleanCardList();
        await CardList.showUsersCards();
      } catch (er) {
        alert("Sorry, try later");
      }
      //скрываем popup
      Modal.destroy();
    }
  }

  formValidate(formData) {
    super.formValidate(formData);
    let { id } = this.fields;

    if (!id || isNaN(id) || +id <= 0) {
      this._VALIDATE_ERRORS["id"] = "Не, сворачивай лавочку, так не подет!";
    }

    return (this._IS_VALID =
      Object.keys(this._VALIDATE_ERRORS).length > 0 ? false : true);
  }
}
